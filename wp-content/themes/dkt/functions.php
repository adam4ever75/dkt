<?php
/**
 * dkt functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package dkt
 */

if ( ! function_exists( 'dkt_setup' ) ) :

	function dkt_setup() {

		load_theme_textdomain( 'dkt', get_template_directory() . '/languages' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'dkt' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;

add_action( 'after_setup_theme', 'dkt_setup' );

function dkt_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'dkt' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'dkt' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'dkt_widgets_init' );

// Remove Admin Bar
show_admin_bar( false );

// Configure/Clean the WordPress defaults
require_once(get_template_directory().'/inc/config.php');

// Customize the WordPress login menu
require_once(get_template_directory().'/inc/login.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/inc/enqueue-scripts.php'); 
