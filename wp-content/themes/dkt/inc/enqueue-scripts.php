<?php
function dkt_scripts() {
  global $wp_styles;
	
	wp_enqueue_style( 'normalize', get_stylesheet_uri() );
	wp_enqueue_style( 'dkt_style', get_template_directory_uri() . '/css/dkt_style.css', array('normalize'), '', 'all' );
	
}
add_action( 'wp_enqueue_scripts', 'dkt_scripts' );